package thesis.project.ImageToText;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.ClipboardManager;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

@SuppressWarnings("deprecation")
public class Ocr extends Activity {
	public static final String DATA_PATH = Environment.getExternalStorageDirectory().toString() + "/ImageToText/";
	public static final String lang = "eng";
	private static final String TAG = "Ocr.java";
	protected static final String PHOTO_TAKEN = "photo_taken";
	protected Button button,copy,gallery;
	protected ImageView image;
	protected EditText field;
	protected boolean _taken;
	protected SharedPreferences preferences;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.ocr);
		preferences = this.getSharedPreferences("thesis.project.ImageToText",Context.MODE_PRIVATE);
        MainActivity.ocr.setBackgroundResource(R.drawable.ocr);
        preferences.edit().putBoolean("thesis.project.ImageToText.OCR.mainoff",true).apply();
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
		image = (ImageView) findViewById(R.id.image);
		field = (EditText) findViewById(R.id.field);
		button = (Button) findViewById(R.id.scan);
		copy = (Button) findViewById(R.id.copy);
		gallery = (Button) findViewById(R.id.load);
		gallery.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v)
			{
				Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i,53);
			}
		});

		field.setText(preferences.getString("thesis.project.ImageToText.OCR.saveocr",""));
        copy.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ClipboardManager clip = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				clip.setText(field.getText());
				Toast.makeText(Ocr.this,"Տեքստը պատճենված է", Toast.LENGTH_SHORT).show();
			}
		});

		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
			    Log.v(TAG, "Starting Camera app");
				startCameraActivity();
			}
		});
		String[] paths = new String[] { DATA_PATH, DATA_PATH + "ITT/" };
		for (String path : paths) {
			File dir = new File(path);
			if (!dir.exists()) {
				if (!dir.mkdirs()) {
					Log.v(TAG, "ERROR: Creation of directory " + path + " on sdcard failed");
					return;
				} else {
					Log.v(TAG, "Created directory " + path + " on sdcard");
				}
			}

		}
		if (!(new File(DATA_PATH + "ITT/" + lang + ".traineddata")).exists()) {
			try {

				AssetManager assetManager = getAssets();
				InputStream in = assetManager.open("ITT/" + lang + ".traineddata");
				OutputStream out = new FileOutputStream(DATA_PATH + "ITT/" + lang + ".traineddata");

				// Transfer bytes from in to out
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				in.close();
				out.close();
				Log.v(TAG, "Copied " + lang + " traineddata");
			} catch (IOException e) {
				Log.e(TAG, "Was unable to copy " + lang + " traineddata " + e.toString());
			}
		}
	}
	@Override
	protected void onDestroy()
	{
		preferences.edit().putString("thesis.project.ImageToText.OCR.saveocr",field.getText().toString()).apply();
		super.onDestroy();
	}
	
	protected void startCameraActivity()
	{
		File Mkdir = new File(DATA_PATH + "/.OCR");
		Mkdir.mkdir();
		File file = new File(DATA_PATH + "/.OCR/ocr.jpg");
		Uri outputFileUri = Uri.fromFile(file);
		final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
		try
		{
		startActivityForResult(intent,0);
		}
		catch(Exception ex)
		{
			//Do Nothing
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		Log.i(TAG, "resultCode: " + resultCode);
		if (requestCode == 0 && resultCode == RESULT_OK)
		{
			String temp = DATA_PATH + "/.OCR/ocr.jpg";
			Toast.makeText(Ocr.this,"Խնդրում եմ սպասել...", Toast.LENGTH_LONG).show();
			preferences.edit().putString("thesis.project.ImageToText.OCR.path",temp).apply();
			onPhotoTaken();
		}
		else if (requestCode == 53 && resultCode == RESULT_OK)
			{
	            Uri selectedImage = data.getData();
	            String[] filePathColumn = {MediaStore.Images.Media.DATA};
	            Cursor cursor = getContentResolver().query(selectedImage,filePathColumn, null, null, null);
	            cursor.moveToFirst();
	            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
	            String picturePath = cursor.getString(columnIndex);
	            cursor.close();
	            preferences.edit().putString("thesis.project.ImageToText.OCR.path",picturePath).apply();
	            onPhotoTaken();
	        }
		else
		{
			Log.v(TAG, "User cancelled");
		}
		
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putBoolean(Ocr.PHOTO_TAKEN, _taken);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		Log.i(TAG, "onRestoreInstanceState()");
		if (savedInstanceState.getBoolean(Ocr.PHOTO_TAKEN)) {
			onPhotoTaken();
		}
	}

	protected void onPhotoTaken()
	{
		AlertDialog.Builder dlgAlert;
        dlgAlert = new AlertDialog.Builder(Ocr.this);
        dlgAlert.setTitle("ImageToText");
		dlgAlert.setCancelable(true);
		dlgAlert.setPositiveButton("Այո",new android.content.DialogInterface.OnClickListener()
		{
			
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				_taken = true;
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inSampleSize = 2;
				String _path = preferences.getString("thesis.project.ImageToText.OCR.path","");
				Bitmap bitmap = BitmapFactory.decodeFile(_path, options);
				try {
					ExifInterface exif = new ExifInterface(_path);
					int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,ExifInterface.ORIENTATION_NORMAL);
					Log.v(TAG, "Orient: " + exifOrientation);
					int rotate = 0;
					switch (exifOrientation) {
					case ExifInterface.ORIENTATION_ROTATE_90:
						rotate = 90;
						break;
					case ExifInterface.ORIENTATION_ROTATE_180:
						rotate = 180;
						break;
					case ExifInterface.ORIENTATION_ROTATE_270:
						rotate = 270;
						break;
					}
					Log.v(TAG, "Rotation: " + rotate);
					if (rotate != 0) {

						// Getting width & height of the given image.
						int w = bitmap.getWidth();
						int h = bitmap.getHeight();

						// Setting pre rotate
						Matrix mtx = new Matrix();
						mtx.preRotate(rotate);

						// Rotating Bitmap
						bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, false);
					}
					// Convert to ARGB_8888, required by tess
					bitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
				} catch (IOException e) {
					Log.e(TAG, "Couldn't correct orientation: " + e.toString());
				}

				image.setImageBitmap(bitmap);

				inspectFromBitmap(bitmap);

			}
		});
		dlgAlert.setNegativeButton("Ոչ",new android.content.DialogInterface.OnClickListener()
		{
			
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				//Do Nothing
			}
		});
		dlgAlert.setMessage("Սկսել գործընթացը");
		dlgAlert.create().show();
	}

	private void inspectFromBitmap(Bitmap bitmap) {
		TextRecognizer textRecognizer = new TextRecognizer.Builder(this).build();
		try {
			if (!textRecognizer.isOperational()) {
				new AlertDialog.
						Builder(this).
						setMessage("Text recognizer could not be set up on your device").show();
				return;
			}

			Frame frame = new Frame.Builder().setBitmap(bitmap).build();
			SparseArray<TextBlock> origTextBlocks = textRecognizer.detect(frame);
			List<TextBlock> textBlocks = new ArrayList<>();
			for (int i = 0; i < origTextBlocks.size(); i++) {
				TextBlock textBlock = origTextBlocks.valueAt(i);
				textBlocks.add(textBlock);
			}
			Collections.sort(textBlocks, new Comparator<TextBlock>() {
				@Override
				public int compare(TextBlock o1, TextBlock o2) {
					int diffOfTops = o1.getBoundingBox().top - o2.getBoundingBox().top;
					int diffOfLefts = o1.getBoundingBox().left - o2.getBoundingBox().left;
					if (diffOfTops != 0) {
						return diffOfTops;
					}
					return diffOfLefts;
				}
			});

			StringBuilder detectedText = new StringBuilder();
			for (TextBlock textBlock : textBlocks) {
				if (textBlock != null && textBlock.getValue() != null) {
					detectedText.append(textBlock.getValue());
					detectedText.append("\n");
				}
			}

			String recognizedText = detectedText.toString();
			Log.v(TAG, "OCRED TEXT: " + detectedText);

			if ( lang.equalsIgnoreCase("eng") ) {
				recognizedText = recognizedText.replaceAll("[^a-zA-Z0-9]+", " ");
			}

			recognizedText = recognizedText.trim();

			if ( recognizedText.length() != 0 ) {
				field.setText(field.getText().toString().length() == 0 ? recognizedText : field.getText() + " " + recognizedText);
				field.setSelection(field.getText().toString().length());
			} else {
				Toast.makeText(Ocr.this,"Նկարում տեքստ չի հայտնաբերվել", Toast.LENGTH_LONG).show();
			}
		}
		finally {
			textRecognizer.release();
		}
	}
}
