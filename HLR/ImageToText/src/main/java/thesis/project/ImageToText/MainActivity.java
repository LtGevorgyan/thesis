package thesis.project.ImageToText;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends Activity {

	
	@Override
	protected void onDestroy()
	{
		preferences.edit().putBoolean("thesis.project.ImageToText.OCR.mainoff",true).apply();
		super.onDestroy();
	}

	@Override
	protected void onPause() 
	{
		preferences.edit().putBoolean("thesis.project.ImageToText.OCR.mainoff",false).apply();
		super.onPause();
	}

	static ImageButton ocr;
	protected SharedPreferences preferences;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
        ocr = (ImageButton) findViewById(R.id.ocr);
		preferences = this.getSharedPreferences("thesis.project.ImageToText",Context.MODE_PRIVATE);
		ocr.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v)
			{
				ocr.setBackgroundResource(R.drawable.ocr_clicked);
				Intent newactivity = new Intent("thesis.project.ImageToText.OCR");
				startActivity(newactivity);
			}
		});
		boolean temp = preferences.getBoolean("thesis.project.ImageToText.OCR.key",false);
         
	}
	
	
	@Override
	protected void onResume()
	{
		boolean finish=preferences.getBoolean("thesis.project.ImageToText.OCR.finish",false);
		boolean mainoff=preferences.getBoolean("thesis.project.ImageToText.OCR.mainoff",false);
		if(finish==true && mainoff==false)
		{
			preferences.edit().putBoolean("thesis.project.ImageToText.OCR.finish",false).apply();
			finish();
		}
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
